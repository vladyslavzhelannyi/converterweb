﻿function convert() {
    var fromToValue = document.getElementById('selectL').value;
    var measureValue = document.getElementById('selectMeasure').value;
    var valueToConvert = document.getElementById('value').value;
    var valueInt = parseInt(valueToConvert);
    var html;
    if (fromToValue == "from kg") {
        switch (measureValue) {
            case "g":
                html = valueInt * 1000;
                break;
            case "carat":
                html = valueInt * 5000;
                break;
            case "eng pound":
                html = valueInt / 0.45359237;
                break;
            case "pound":
                html = valueInt * 2.20462;
                break;
            case "stone":
                html = valueInt * 0.157473;
                break;
            case "rus pound":
                html = valueInt / 0.40951241;
                break;
        }
    }
    else {
        switch (measureValue) {
            case "g":
                html = valueInt / 1000;
                break;
            case "carat":
                html = valueInt / 5000;
                break;
            case "eng pound":
                html = valueInt * 0.45359237;
                break;
            case "pound":
                html = valueInt / 2.20462;
                break;
            case "stone":
                html = valueInt / 0.157473;
                break;
            case "rus pound":
                html = valueInt * 0.40951241;
                break;
        }
    }
    document.getElementById('result').innerHTML = html;
}

document.getElementById('convert').addEventListener('click', convert);