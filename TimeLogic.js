﻿function convert() {
    var fromToValue = document.getElementById('selectL').value;
    var measureValue = document.getElementById('selectMeasure').value;
    var valueToConvert = document.getElementById('value').value;
    var valueInt = parseInt(valueToConvert);
    var html;
    if (fromToValue == "from sec") {
        switch (measureValue) {
            case "Min":
                html = valueInt / 60;
                break;
            case "Hour":
                html = valueInt / 3600;
                break;
            case "Day":
                html = (valueInt / 3600) / 24;
                break;
            case "Week":
                html = ((valueInt / 3600) / 24) / 7;
                break;
            case "Month":
                html = (((valueInt / 3600) / 24) / 7) / 30;
                break;
            case "Astronomical Year":
                html = ((((valueInt / 3600) / 24) / 7) / 30) / 365;
                break;
        }
    }
    else {
        switch (measureValue) {
            case "Min":
                html = valueInt * 60;
                break;
            case "Hour":
                html = valueInt * 3600;
                break;
            case "Day":
                html = (valueInt * 3600) * 24;
                break;
            case "Week":
                html = ((valueInt * 3600) * 24) * 7;
                break;
            case "Month":
                html = (((valueInt * 3600) * 24) * 7) * 30;
                break;
            case "Astronomical Year":
                html = ((((valueInt * 3600) * 24) * 7) * 30) * 365;
                break;
        }
    }
    document.getElementById('result').innerHTML = html;
}

document.getElementById('convert').addEventListener('click', convert);