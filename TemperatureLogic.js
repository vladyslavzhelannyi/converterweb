﻿function convert() {
    var fromToValue = document.getElementById('selectL').value;
    var measureValue = document.getElementById('selectMeasure').value;
    var valueToConvert = document.getElementById('value').value;
    var valueInt = parseInt(valueToConvert);
    var html;
    if (fromToValue == "from C") {
        switch (measureValue) {
            case "K":
                html = valueInt + 237.15;
                break;
            case "F":
                html = (valueInt * 9 / 5) + 32;
                break;
            case "Re":
                html = valueInt * 0.8;
                break;
            case "Ro":
                html = valueInt * 0.525 + 7.5;
                break;
            case "Ra":
                html = valueInt * 1.8 + 491.67;
                break;
            case "N":
                html = valueInt * 0.33;
                break;
            case "Dv":
                html = valueInt * 1.5 - 100;
                break;
        }
    }
    else {
        switch (measureValue) {
            case "K":
                html = valueInt - 237.15;
                break;
            case "F":
                html = (valueInt - 32) / 9 * 5;
                break;
            case "Re":
                html = valueInt / 0.8;
                break;
            case "Ro":
                html = (valueInt - 7.5) / 0.525;
                break;
            case "Ra":
                html = (valueInt - 491.67) / 1.8;
                break;
            case "N":
                html = valueInt / 0.33;
                break;
            case "Dv":
                html = (valueInt + 100) / 1.5;
                break;
        }
    }
    document.getElementById('result').innerHTML = html;
}

document.getElementById('convert').addEventListener('click', convert);