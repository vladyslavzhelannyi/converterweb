﻿function convert() {
    var fromToValue = document.getElementById('selectL').value;
    var measureValue = document.getElementById('selectMeasure').value;
    var valueToConvert = document.getElementById('value').value;
    var valueInt = parseInt(valueToConvert);
    var html;
    if (fromToValue == "from l") {
        switch (measureValue) {
            case "m^3":
                html = valueInt * 0.001;
                break;
            case "gallon":
                html = valueInt * 0.264172;
                break;
            case "pint":
                html = valueInt * 1.75975;
                break;
            case "quart":
                html = valueInt * 0.879877;
                break;
            case "barrel":
                html = valueInt / 158.988;
                break;
            case "cubic foot":
                html = valueInt * 0.0353147;
                break;
            case "cubic inch":
                html = valueInt * 61.0237;
                break;
        }
    }
    else {
        switch (measureValue) {
            case "m^3":
                html = valueInt / 0.001;
                break;
            case "gallon":
                html = valueInt / 0.264172;
                break;
            case "pint":
                html = valueInt / 1.75975;
                break;
            case "quart":
                html = valueInt / 0.879877;
                break;
            case "barrel":
                html = valueInt * 158.988;
                break;
            case "cubic foot":
                html = valueInt / 0.0353147;
                break;
            case "cubic inch":
                html = valueInt / 61.0237;
                break;
        }
    }
    document.getElementById('result').innerHTML = html;
}

document.getElementById('convert').addEventListener('click', convert);