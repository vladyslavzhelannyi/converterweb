﻿function convert() {
    var fromToValue = document.getElementById('selectL').value;
    var measureValue = document.getElementById('selectMeasure').value;
    var valueToConvert = document.getElementById('value').value;
    var valueInt = parseInt(valueToConvert);
    var html;
    if (fromToValue == "from m") {
        switch (measureValue) {
            case "km":
                html = valueInt * 0.001;
                break;
            case "mile":
                html = valueInt * 0.000621371;
                break;
            case "nautical mile":
                html = valueInt * 0.000539957;
                break;
            case "cable":
                html = valueInt / 219.456;
                break;
            case "league":
                html = valueInt / 4179.5;
                break;
            case "foot":
                html = valueInt * 3.28084;
                break;
            case "yard":
                html = valueInt * 1.09361;
                break;
        }
    }
    else {
        switch (measureValue) {
            case "km":
                html = valueInt / 0.001;
                break;
            case "mile":
                html = valueInt / 0.000621371;
                break;
            case "nautical mile":
                html = valueInt / 0.000539957;
                break;
            case "cable":
                html = valueInt * 219.456;
                break;
            case "league":
                html = valueInt * 4179.5;
                break;
            case "foot":
                html = valueInt / 3.28084;
                break;
            case "yard":
                html = valueInt / 1.09361;
                break;
        }
    }
    document.getElementById('result').innerHTML = html;
}

document.getElementById('convert').addEventListener('click', convert);